<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="custom-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="orderListTitle" bundle="${loc}" key="locale.title.order.list" />
<fmt:message var="orderNotAvailable" bundle="${loc}" key="locale.error.order.not.available" />
<title>${orderListTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	<c:if test="${requestScope.invalidData}">
			<span class="error">${orderNotAvailable }</span>
			<br>
	</c:if>
	<ctg:show-order orderList="${sessionScope.orderList }"/>
	<form id="refreshList" action="Controller" method="post">
		<input type="hidden" name="command" value="show_order_list" /> 
	</form>
	</div>
	<%@ include file="include/footer.jsp"%>
	<script src="js/refresh_order_list.js" ></script>
</body>
</html>