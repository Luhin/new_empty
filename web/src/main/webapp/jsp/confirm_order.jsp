<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="clientLocation" bundle="${loc}"
	key="locale.client.location" />
<fmt:message var="processTitle" bundle="${loc}"
	key="locale.process.order.title" />
<fmt:message var="orderProcess" bundle="${loc}"
	key="locale.order.process" />
<fmt:message var="noCarsMessage" bundle="${loc}"
	key="locale.message.no.cars" />
<fmt:message var="tryAgain" bundle="${loc}"
	key="locale.message.try.again" />
<fmt:message var="toMainPage" bundle="${loc}"
	key="locale.button.to.main.page" />
<fmt:message var="awaitingTime" bundle="${loc}" key="locale.message.time.awaiting" />
<fmt:message var="driverName" bundle="${loc}" key="locale.message.driver.name" />
<fmt:message var="carModel" bundle="${loc}" key="locale.message.driver.car" />
<fmt:message var="rating" bundle="${loc}" key="locale.message.driver.rating" />
<fmt:message var="price" bundle="${loc}" key="locale.message.price" />
<fmt:message var="confirmOrder" bundle="${loc}" key="locale.button.confirm.order" />
<fmt:message var="cancelOrder" bundle="${loc}" key="locale.button.cancel.order" />
<title>${processTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	<div id="orderStatus">${orderProcess }</div>
	<div id="noCarsDiv" style="display: none">
		${noCarsMessage } <br>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="send_order" /> <input class="btn btn-primary"
				type="submit" value="${tryAgain }" />
		</form>
		<br>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="to-main-page" /> <input class="btn"
				type="submit" value="${toMainPage }" />
		</form>
	</div>
	<div id="orderAccepted" style="display:none;">
		<table class='table table-bordered table-condensed' style="width:25%;">
		<tr><td>${awaitingTime }</td><td><span id="awaitingTime"></span></td></tr>
		<tr><td>${driverName }</td><td><span id="driverName"></span></td></tr>
		<tr><td>${rating }</td><td><span id="rating"></span></td></tr>
		<tr><td>${carModel }</td><td><span id="carModel"></span></td></tr>
		<tr><td>${price }</td><td><span id="price"></span></td></tr>
		</table>
		<br>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="confirm-order" /> 
			<input class="btn btn-primary" type="submit" value="${confirmOrder }" />
		</form>
		<br>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="cancel-order" /> <input class="btn btn-info"
				type="submit" value="${cancelOrder }" />
		</form>
	</div>
	<span id=userURL style="display:none">${sessionScope.userURL }</span>
	</div>
	<%@ include file="include/footer.jsp"%>
	<script src="js/check_order_acceptiance.js"></script>
</body>
</html>