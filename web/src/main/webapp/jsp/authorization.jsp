<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="userField" bundle="${loc}" key="locale.user.field" />
<fmt:message var="passField" bundle="${loc}" key="locale.pass.field" />
<fmt:message var="loginButton" bundle="${loc}" key="locale.login.button" />
<fmt:message var="invalidData" bundle="${loc}" key="locale.invalid.data" />
<fmt:message var="authorizationButton" bundle="${loc}"
	key="locale.authorization.button" />
<fmt:message var="greeting" bundle="${loc}" key="locale.greeting" />
<fmt:message var="loginTitle" bundle="${loc}" key="locale.title.login" />
<fmt:message var="signUp" bundle="${loc}" key="locale.button.sign.up" />
<fmt:message var="choiseOr" bundle="${loc}" key="locale.message.choise.or" />
<fmt:message var="enterName" bundle="${loc}" key="locale.entername" />
<fmt:message var="enterLogin" bundle="${loc}" key="locale.user.field" />
<fmt:message var="enterPassword" bundle="${loc}" key="locale.pass.field" />
<fmt:message var="repeatPassword" bundle="${loc}" key="locale.repeatpassword" />
<fmt:message var="enterEmail" bundle="${loc}" key="locale.enteremail" />
<fmt:message var="enterCardCode" bundle="${loc}" key="locale.message.enter.card.code" />
<fmt:message var="nameInfo" bundle="${loc}" key="locale.message.name.info" />
<fmt:message var="loginInfo" bundle="${loc}" key="locale.message.login.info" />
<fmt:message var="passInfo" bundle="${loc}" key="locale.message.pass.info" />
<fmt:message var="emailInfo" bundle="${loc}" key="locale.message.email.info" />
<fmt:message var="cardInfo" bundle="${loc}" key="locale.message.card.info" />
<fmt:message var="send" bundle="${loc}" key="locale.button.send" />
<fmt:message var="requiredFieldMessage" bundle="${loc}" key="locale.message.required.field" />
<fmt:message var="requiredFieldChar" bundle="${loc}" key="locale.message.required.character" />

<title>${loginTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div class="content">
	<div class="authorization">
		${greeting } <br />
		<c:if test="${requestScope.invalidData}">
			<span class="error">${invalidData }</span>
		</c:if>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="authorization" />
			${userField } <br /> <input type="text" name="login" value=""
				required pattern="[a-zA-Z0-9]{4,20}" /> <br /> ${passField } <br /> <input
				type="password" name="password" value="" required
				pattern="[a-zA-Z0-9]{4,20}" /> <br /> <input class="btn btn-primary" type="submit"
				value="${loginButton }" />
		</form>
		${choiseOr }
		<br>
		<input class="btn btn-info" id="show_div" type="button" value="${signUp }" />
		</div>
		<c:choose>
  		<c:when test="${not empty requestScope.invalidRegistrData}">
   			 <div id="hidden_div" style="display:block">
 		 </c:when>
 		<c:otherwise>
    		<div id="hidden_div" style="display:none">
   		</c:otherwise>
		</c:choose>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="sign-up-client" />
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
			<c:if test="${not empty requestScope.invalidRegistrData}">
			<span class="error">${requestScope.invalidRegistrData }</span>
			<br>
			</c:if>
			${enterName }${requiredFieldChar }<br>
			<input type="text" name="registrName" required pattern="[a-zA-Zа-яёА-ЯЁ\s]{2,20}" value="" /> ${nameInfo }
			<br>
			${enterLogin }${requiredFieldChar }<br>
			<input type="text" name="registrLogin" required pattern="[a-zA-Z0-9]{4,20}" value="" /> ${loginInfo }
			<br>
			${enterPassword }${requiredFieldChar }<br>
			<input type="password" name="registrPass" required pattern="[a-zA-Z0-9]{4,20}" value="" /> ${passInfo }
			<br>
			${repeatPassword }${requiredFieldChar }<br>
			<input type="password" name="registrRepeatPass" required pattern="[a-zA-Z0-9]{4,20}" value="" />
			<br>
			${enterEmail }${requiredFieldChar }<br>
			<input type="text" name="registrEmail" required pattern=".+@.+" value="" /> ${emailInfo }
			<br>
			${enterCardCode }${requiredFieldChar }<br>
			<input type="text" name="registrCardNumber" required pattern="[0-9]{3,3}" value="" /> ${cardInfo }
			<br>
			${requiredFieldMessage }
			<br>
			<input class="btn btn-primary"
				type="submit" value="${send }" />
		</form>
	</div>
	</div>
	<%@ include file="include/footer.jsp"%>
	<script src="js/show_hidden_div.js" ></script>
</body>
</html>