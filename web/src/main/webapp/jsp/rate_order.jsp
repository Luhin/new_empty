<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle var="loc" basename="localization.locale" />
<fmt:message var="orderResultTitle" bundle="${loc}"
	key="locale.title.order.result" />
<fmt:message var="orderList" bundle="${loc}" key="locale.order.list" />
<fmt:message var="toMainPage" bundle="${loc}" key="locale.on.main" />
<fmt:message var="orderCompleted" bundle="${loc}"
	key="locale.message.success.order" />
<fmt:message var="rateOrder" bundle="${loc}"
	key="locale.message.rate.order" />
<fmt:message var="comment" bundle="${loc}"
	key="locale.message.create.comment" />
<fmt:message var="commentInfo" bundle="${loc}"
	key="locale.message.comment.info" />
<fmt:message var="rateDriver" bundle="${loc}"
	key="locale.message.rate.driver" />
<fmt:message var="ratingInfo" bundle="${loc}"
	key="locale.message.info.rating" />
<fmt:message var="sendButton" bundle="${loc}" key="locale.button.send" />
<fmt:message var="rateClient" bundle="${loc}" key="locale.message.rate.client" />
<title>${orderResultTitle }</title>
</head>
<body>
	<%@ include file="include/header.jsp"%>
	<div id="successOrder" class="content">
		${orderCompleted }<br> ${rateOrder } <br>
		<c:if test="${not empty requestScope.invalidData }">
			requestScope.invalidData
		</c:if>
		${comment }<br>
		${commentInfo }
		<br>
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="rate-order" />
			<input type="hidden" name="pageUnique" value="${sessionScope.pageUnique }"/>
			<textarea name="comment" value="" required pattern="[a-zA-Zа-яёА-ЯЁ\\s0-9!,.?]{1,250}" cols="40" rows="4"></textarea>
			<br>
			<c:if test="${sessionScope.userRole eq 'driver'}">
				${rateClient }
			</c:if>
			<c:if test="${sessionScope.userRole eq 'client'}">
				${rateDriver}
			</c:if>
			<br>
			${ratingInfo }
			<br>
			<input name="rating" type="text" value="" required pattern="[1-5]{1,1}"/>
			<br>
			<input class="btn btn-success"	type="submit" value="${sendButton }" />
		</form>
	</div>
	<%@ include file="include/footer.jsp"%>
</body>
</html>