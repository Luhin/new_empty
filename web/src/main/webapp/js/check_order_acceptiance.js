;
function createXMLHttpRequest() {
	var request = false;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		request = new ActiveXObject("Msxml2.XMLHTTP");
	}
	return request;
};
var request = createXMLHttpRequest();
function getAnswer() {
	if (request.readyState == 4 && request.status == 200) {
		var orderStatus = document.getElementById("orderStatus");
		orderStatus.style.display = "none";
		if (request.responseText == "noCars") {
			document.getElementById("noCarsDiv").style.display = "block";
		} else if (request.responseText == "orderCancelled") {
			return;
		} else {
			var answer = request.responseText;
			var result = JSON.parse(answer);
			document.getElementById("orderAccepted").style.display = "block";
			document.getElementById("awaitingTime").innerHTML = result.time;
			document.getElementById("driverName").innerHTML = result.name;
			document.getElementById("rating").innerHTML = result.rating;
			document.getElementById("carModel").innerHTML = result.car;
			document.getElementById("price").innerHTML = result.price;
		}
	}
}
function checkOrderAcceptiance() {
	var url = document.getElementById("userURL").childNodes[0].nodeValue;
	request.open("POST", url, true);
	request.setRequestHeader("Content-Type",
			"application/x-www-form-urlencoded");
	request.send("command=wait-accept-order");
	request.onreadystatechange = getAnswer;
};
window.onload = checkOrderAcceptiance();