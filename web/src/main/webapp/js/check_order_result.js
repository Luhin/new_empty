;
function createXMLHttpRequest() {
	var request = false;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		request = new ActiveXObject("Msxml2.XMLHTTP");
	}
	return request;
};
var request = createXMLHttpRequest();
function getAnswer() {
	if (request.readyState == 4 && request.status == 200) {
		var orderStatus = document.getElementById("answerWaiting");
		orderStatus.style.display = "none";
		if (request.responseText == "orderCancelled") {
			document.getElementById("orderCancelled").style.display = "block";
		} else if (request.responseText == "successOrder") {
			document.getElementById("successOrder").style.display = "block";
		}
	}
}
function checkOrderResult() {
	var url = document.getElementById("userURL").childNodes[0].nodeValue;
	request.open("POST", url, true);
	request.setRequestHeader("Content-Type",
			"application/x-www-form-urlencoded");
	request.send("command=check-order-result");
	request.onreadystatechange = getAnswer;
};
window.onload = checkOrderResult();