package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ConfirmOrderCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Client client = (Client) session.getAttribute(AttributeName.CLIENT);
        try {
            if (OrderService.getInstance().confirmOrder(client.getUserId())) {
                session.setAttribute(AttributeName.LAST_PAGE, PageName.RATE_ORDER);
                return PageName.RATE_ORDER;
            } else {
                request.setAttribute(AttributeName.MAKE_ORDER_ERROR, true);
                session.setAttribute(AttributeName.LAST_PAGE, PageName.CREATE_ORDER);
                return PageName.CREATE_ORDER;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
