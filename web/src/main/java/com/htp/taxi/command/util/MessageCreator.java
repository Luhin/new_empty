package com.htp.taxi.command.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Владимир on 29.11.2016.
 */
public class MessageCreator {
    private final static String LOCALE_PROPERTIES = "localization.locale";

    public static String createErrorMessage(String errorType, String userLocale) {
        Locale locale;
        if (userLocale == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(userLocale);
        }
        ResourceBundle bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
        String errorMessage = bundle.getString(errorType);
        return errorMessage;
    }
}
