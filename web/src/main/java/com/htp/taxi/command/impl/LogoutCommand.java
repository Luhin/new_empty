package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;

public class LogoutCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(AttributeName.CLIENT);
        try {
            OrderService.getInstance().deleteOrder(user.getUserId());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.invalidate();
        request.getSession().setAttribute(AttributeName.LAST_PAGE, PageName.AUTHORIZATION);
        return PageName.AUTHORIZATION;
    }
}
