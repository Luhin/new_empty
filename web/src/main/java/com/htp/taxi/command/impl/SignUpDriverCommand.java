package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.util.MessageCreator;
import com.htp.taxi.command.PageName;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.DriverService;
import com.htp.taxi.service.impl.UserService;
import com.htp.taxi.validation.Validation;

public class SignUpDriverCommand implements ICommand {
    private final static String LOGIN_EXISTS = "locale.error.login.exists";
    private final static String INCORRECT_NAME = "locale.error.incorrect.name";
    private final static String INCORRECT_LOGIN = "locale.error.incorrect.login";
    private final static String INCORRECT_PASSWORD = "locale.error.incorrect.password";
    private final static String PASSWORDS_DONT_MATCH = "locale.error.pass.dont.match";
    private final static String INCORRECT_IMAIL = "locale.error.incorrect.email";
    private final static String INCORRECT_CAR_MODEL = "locale.error.incorrect.car.model";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
        String registrName = request.getParameter(AttributeName.REGISTR_NAME);
        String registrLogin = request.getParameter(AttributeName.REGISTR_LOGIN);
        String registrPass = request.getParameter(AttributeName.REGISTR_PASS);
        String registrRepeatPass = request.getParameter(AttributeName.REGISTR_REPEAT_PASS);
        String registrEmail = request.getParameter(AttributeName.REGISTR_EMAIL);
        String registrCarModel = request.getParameter(AttributeName.REGISTR_CAR_MODEL);
        String errorMessage = validateData(registrName, registrLogin, registrPass, registrRepeatPass, registrEmail,
                registrCarModel, userLocale);
        if (errorMessage != null) {
            request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
            return PageName.USER_PAGE;
        }
        try {
            if (UserService.getInstance().matchExistLogin(registrLogin)) {
                errorMessage = MessageCreator.createErrorMessage(LOGIN_EXISTS, userLocale);
                request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
                return PageName.USER_PAGE;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        try {
            DriverService.getInstance().signUpDriver(registrName, registrLogin, registrPass, registrEmail,
                    registrCarModel);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
        return PageName.USER_PAGE;
    }

    private String validateData(String registrName, String registrLogin, String registrPass, String repeatedPass,
                                String registrEmail, String registrCarModel, String userLocale) {
        Validation validation = Validation.getInstance();
        if (!validation.validateName(registrName)) {
            return MessageCreator.createErrorMessage(INCORRECT_NAME, userLocale);
        }
        if (!validation.validateLogin(registrLogin)) {
            return MessageCreator.createErrorMessage(INCORRECT_LOGIN, userLocale);
        }
        if (!validation.validatePassword(registrPass)) {
            return MessageCreator.createErrorMessage(INCORRECT_PASSWORD, userLocale);
        }
        if (!validation.validatePassEqals(registrPass, repeatedPass)) {
            return MessageCreator.createErrorMessage(PASSWORDS_DONT_MATCH, userLocale);
        }
        if (!validation.validateEmail(registrEmail)) {
            return MessageCreator.createErrorMessage(INCORRECT_IMAIL, userLocale);
        }
        if (!validation.validateCarModel(registrCarModel)) {
            return MessageCreator.createErrorMessage(INCORRECT_CAR_MODEL, userLocale);
        } else {
            return null;
        }
    }
}
