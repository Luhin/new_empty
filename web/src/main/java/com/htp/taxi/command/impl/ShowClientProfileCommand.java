package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.UserService;

public class ShowClientProfileCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
        Client client = (Client) session.getAttribute(AttributeName.CLIENT);
        try {
            Client updated = (Client) UserService.getInstance().updateClientData(client.getUserId());
            session.setAttribute(AttributeName.CLIENT, updated);
            return PageName.CLIENT_PROFILE;
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
