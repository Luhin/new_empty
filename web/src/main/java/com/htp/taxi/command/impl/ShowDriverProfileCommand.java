package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.UserService;

public class ShowDriverProfileCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Driver driver = (Driver) session.getAttribute(AttributeName.CLIENT);
        try {
            Driver updated = (Driver) UserService.getInstance().updateClientData(driver.getUserId());
            session.setAttribute(AttributeName.CLIENT, updated);
            session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
            return PageName.DRIVER_PROFILE;
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
