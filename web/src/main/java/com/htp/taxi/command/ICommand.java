package com.htp.taxi.command;

import com.htp.taxi.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface ICommand {
    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}