package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.util.MessageCreator;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;
import com.htp.taxi.validation.Validation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class EditCommentCommand implements ICommand {
    private final static String INCORRECT_COMMENT = "locale.error.incorrect.comment";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        int orderId = (int) session.getAttribute(AttributeName.ORDER_ID);
        String newComment = request.getParameter(AttributeName.NEW_COMMENT);
        String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
        if (!Validation.getInstance().validateComment(newComment)) {
            String errorMessage = MessageCreator.createErrorMessage(INCORRECT_COMMENT, userLocale);
            request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
            return PageName.AUTHORIZATION;
        }
        User user = (User) session.getAttribute(AttributeName.CLIENT);
        try {
            OrderService.getInstance().editUserComment(user, orderId, newComment);
            if (user instanceof Client) {
                session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
                request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
                return PageName.CLIENT_PROFILE;
            } else {
                session.setAttribute(AttributeName.LAST_PAGE, PageName.DRIVER_PROFILE);
                request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
                return PageName.DRIVER_PROFILE;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
