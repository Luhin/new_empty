package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.exception.CommandException;

public class EditingCommentCommand implements ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        int orderId = Integer.parseInt(request.getParameter(AttributeName.ORDER_ID));
        session.setAttribute(AttributeName.ORDER_ID, orderId);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.EDIT_COMMENT);
        return PageName.EDIT_COMMENT;
    }
}
