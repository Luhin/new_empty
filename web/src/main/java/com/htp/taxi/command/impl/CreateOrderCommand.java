package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.util.MessageCreator;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Address;
import com.htp.taxi.entity.Client;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.ClientService;
import com.htp.taxi.service.impl.OrderService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class CreateOrderCommand implements ICommand {
    private final static String BANNED_USER = "locale.error.banned";
    private final static String NOT_ENOUG_BALANCE = "locale.error.not.enough.money";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Client client = (Client) session.getAttribute(AttributeName.CLIENT);
        String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
        ClientService clientService = ClientService.getInstance();
        String errorMessage;
        if (!clientService.isBalanceEnough(client)) {
            errorMessage = MessageCreator.createErrorMessage(NOT_ENOUG_BALANCE, userLocale);
            request.setAttribute(AttributeName.ERROR_MESSAGE, errorMessage);
            session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
            return PageName.USER_PAGE;
        }
        if (client.isBanned()) {
            errorMessage = MessageCreator.createErrorMessage(BANNED_USER, userLocale);
            request.setAttribute(AttributeName.ERROR_MESSAGE, errorMessage);
            session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
            return PageName.USER_PAGE;
        }
        Cookie cookies[] = request.getCookies();
        String previousStreet = null;
        int previousHouseNumber = 0;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(AttributeName.PREVIOUS_STREET)) {
                    previousStreet = cookie.getValue();
                }
                if (cookie.getName().equals(AttributeName.PREVIOUS_HOUSE_NUMBER)) {
                    previousHouseNumber = Integer.parseInt(cookie.getValue());
                }
            }
            session.setAttribute(AttributeName.PREVIOUS_STREET, previousStreet);
            session.setAttribute(AttributeName.PREVIOUS_HOUSE_NUMBER, previousHouseNumber);
        }
        List<String> streetList;
        Address currentAddress;
        try {
            currentAddress = clientService.determineClientPosition();
            streetList = OrderService.getInstance().makeStreetList();
            session.setAttribute(AttributeName.CURRENT_STREET, currentAddress);
            session.setAttribute(AttributeName.STREET_LIST, streetList);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.LAST_PAGE, PageName.CREATE_ORDER);
        return PageName.CREATE_ORDER;
    }
}
