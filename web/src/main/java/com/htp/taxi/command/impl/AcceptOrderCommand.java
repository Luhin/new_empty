package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.Order;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

public class AcceptOrderCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Driver driver = (Driver)session.getAttribute(AttributeName.CLIENT);
        int clientId = Integer.parseInt(request.getParameter(AttributeName.CLIENT_ID));
        session.setAttribute(AttributeName.ORDER_ID, clientId);
        try {
            if (OrderService.getInstance().acceptOrder(clientId, driver)) {
                String userURL = request.getRequestURL().toString();
                session.setAttribute(AttributeName.USER_URL, userURL);
                session.setAttribute(AttributeName.LAST_PAGE, PageName.RESULT_ORDER);
                return PageName.RESULT_ORDER;
            } else {
                ArrayList<Order> orderList = OrderService.getInstance().showOrders();
                session.setAttribute(AttributeName.ORDER_LIST, orderList);
                session.setAttribute(AttributeName.LAST_PAGE, PageName.ORDER_LIST);
                request.setAttribute(AttributeName.INVALID_DATA, true);
                return PageName.ORDER_LIST;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
