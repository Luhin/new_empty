package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.util.MessageCreator;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.User;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;
import com.htp.taxi.validation.Validation;

public class RateOrderCommand implements ICommand {
    private final static String INCORRECT_COMMENT = "locale.error.incorrect.comment";
    private final static String INCORRECT_RATING = "locale.error.incorrect.rating";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        String comment = request.getParameter(AttributeName.COMMENT);
        String rating = request.getParameter(AttributeName.RATING);
        String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
        String errorMessage = validateData(comment, rating, userLocale);
        if (errorMessage != null) {
            request.setAttribute(AttributeName.INVALID_DATA, errorMessage);
            return PageName.RESULT_ORDER;
        }
        User user = (User) session.getAttribute(AttributeName.CLIENT);
        try {
            OrderService.getInstance().rateOrder(user, comment, rating);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        request.setAttribute(AttributeName.SUCCESS_ORDER, true);
        session.setAttribute(AttributeName.LAST_PAGE, PageName.USER_PAGE);
        return PageName.USER_PAGE;
    }

    private String validateData(String comment, String rating, String userLocale) {
        Validation validation = Validation.getInstance();
        if (!validation.validateComment(comment)) {
            return MessageCreator.createErrorMessage(INCORRECT_COMMENT, userLocale);
        }
        if (!validation.validateRating(rating)) {
            return MessageCreator.createErrorMessage(INCORRECT_RATING, userLocale);
        } else {
            return null;
        }
    }
}
