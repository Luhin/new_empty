package com.htp.taxi.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.ClientService;
import com.htp.taxi.validation.Validation;

public class ReplenishBalanceCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        int replenishSum = Integer.parseInt(request.getParameter(AttributeName.REPLENISH_SUM));
        String cardNumber = request.getParameter(AttributeName.CARD_NUMBER);
        Client client = (Client) session.getAttribute(AttributeName.CLIENT);
        if (!Validation.getInstance().validateCardNum(cardNumber)) {
            request.setAttribute(AttributeName.INVALID_DATA, true);
            return PageName.AUTHORIZATION;
        }
        try {
            if (ClientService.getInstance().replenishBalance(client, replenishSum, cardNumber)) {
                request.setAttribute(AttributeName.SUCCESS_OPERATION, true);
            } else {
                request.setAttribute(AttributeName.INVALID_DATA, true);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        session.setAttribute(AttributeName.LAST_PAGE, PageName.CLIENT_PROFILE);
        return PageName.CLIENT_PROFILE;
    }
}
