package com.htp.taxi.command.impl;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.PageName;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CheckOrderResultCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        int orderId = (int) session.getAttribute(AttributeName.ORDER_ID);
        String result;
        try {
            result = OrderService.getInstance().checkOrderResult(orderId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        try {
            response.getWriter().write(result);
        } catch (IOException e1) {
            throw new CommandException(e1);
        }
        return PageName.AJAX;
    }
}
