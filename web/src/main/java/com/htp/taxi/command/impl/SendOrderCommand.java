package com.htp.taxi.command.impl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.command.ICommand;
import com.htp.taxi.command.util.MessageCreator;
import com.htp.taxi.command.PageName;
import com.htp.taxi.entity.Address;
import com.htp.taxi.entity.Client;
import com.htp.taxi.exception.CommandException;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.OrderService;
import com.htp.taxi.validation.Validation;

public class SendOrderCommand implements ICommand {
    private final static String INCORRECT_HOUSE_NUM = "locale.error.incorrect.house.num";
    private final static int COOKIE_MAX_AGE = 2592000;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Client client = (Client) session.getAttribute(AttributeName.CLIENT);
        Address positionAddress = (Address) session.getAttribute(AttributeName.CURRENT_STREET);
        int positionHouseNumber = positionAddress.getHouseNum();
        String destination = request.getParameter(AttributeName.DESTINATION);
        String houseNumber = request.getParameter(AttributeName.HOUSE_NUMBER);
        if (destination != null) {
            session.setAttribute(AttributeName.DESTINATION, destination);
            session.setAttribute(AttributeName.HOUSE_NUMBER, houseNumber);
        } else {
            destination = (String) session.getAttribute(AttributeName.DESTINATION);
            houseNumber = (String) session.getAttribute(AttributeName.HOUSE_NUMBER);
        }
        String userLocale = (String) session.getAttribute(AttributeName.LOCALE);
        if (!Validation.getInstance().validateHouseNumber(houseNumber)) {
            String errorMessage = MessageCreator.createErrorMessage(INCORRECT_HOUSE_NUM, userLocale);
            request.setAttribute(AttributeName.INVALID_REGISTR_DATA, errorMessage);
            return PageName.CREATE_ORDER;
        }
        Cookie previousAddressCookie = new Cookie(AttributeName.PREVIOUS_STREET, destination);
        previousAddressCookie.setMaxAge(COOKIE_MAX_AGE);
        response.addCookie(previousAddressCookie);
        Cookie previousHouseCookie = new Cookie(AttributeName.PREVIOUS_HOUSE_NUMBER, houseNumber);
        previousHouseCookie.setMaxAge(COOKIE_MAX_AGE);
        response.addCookie(previousHouseCookie);
        try {
            OrderService.getInstance().createOrder(client, positionAddress.getStreetName(), positionHouseNumber, destination,
                    houseNumber);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        String userURL = request.getRequestURL().toString();
        session.setAttribute(AttributeName.USER_URL, userURL);
        request.getSession().setAttribute(AttributeName.LAST_PAGE, PageName.CONFIRM_ORDER);
        return PageName.CONFIRM_ORDER;
    }
}
