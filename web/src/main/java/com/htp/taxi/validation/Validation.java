package com.htp.taxi.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates all data with corresponding pattern, which was entered by users.
 *
 * @author Uladzimir Luhin
 *
 */
public class Validation {
    /**
     * Instance of the singlton Validation.
     */
    private final static Validation INSTANCE = new Validation();
    /**
     * Regular expression to the correct user name
     */
    private final static String NAME_REG_EX = "[a-zA-Zа-яёА-ЯЁ\\s]{2,20}";
    /**
     * Regular expression to the correct user login
     */
    private final static String LOGIN_REG_EX = "[a-zA-Z0-9]{4,20}";
    /**
     * Regular expression to the correct user password
     */
    private final static String PASSWORD_REG_EX = "[a-zA-Z0-9]{4,20}";
    /**
     * Regular expression to the correct user email
     */
    private final static String EMAIL_REG_EX = ".+@.+";
    /**
     * Regular expression to the correct user card number
     */
    private final static String CARD_NUM_REG_EX = "[0-9]{3,3}";
    /**
     * Regular expression to the correct user comment
     */
    private final static String COMMENT_REG_EX = "[a-zA-Zа-яёА-ЯЁ\\s0-9!,.?]{1,250}";
    /**
     * Regular expression to the correct user house number
     */
    private final static String HOUSE_NUM_REG_EX = "[0-9]{1,3}";
    /**
     * Regular expression to the correct user rating
     */
    private final static String RATING_REG_EX = "[1-5]{1,1}";
    /**
     * Regular expression to the correct user car model
     */
    private final static String CAR_MODEL_REG_EX = "[0-9a-zA-Zа-яёА-ЯЁ-ߨ]{1,20}";

    private Validation() {

    }

    public static Validation getInstance() {
        return INSTANCE;
    }

    /**
     * Validates name with the corresponding regular expression
     *
     * @param name
     * @return
     */
    public boolean validateName(String name) {
        Pattern pattern = Pattern.compile(NAME_REG_EX);
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /**
     * Validates login with the corresponding regular expression
     *
     * @param login
     * @return
     */
    public boolean validateLogin(String login) {
        Pattern pattern = Pattern.compile(LOGIN_REG_EX);
        Matcher matcher = pattern.matcher(login);
        return matcher.matches();
    }

    /**
     * Validates password with the corresponding regular expression
     *
     * @param pass
     * @return
     */
    public boolean validatePassword(String pass) {
        Pattern pattern = Pattern.compile(PASSWORD_REG_EX);
        Matcher matcher = pattern.matcher(pass);
        return matcher.matches();
    }

    /**
     * Validates password for equality to the repeated password
     *
     * @param pass
     * @param repeatPass
     * @return
     */
    public boolean validatePassEqals(String pass, String repeatPass) {
        return pass.equals(repeatPass);
    }

    /**
     * Validates email with the corresponding regular expression
     *
     * @param email
     * @return
     */
    public boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REG_EX);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Validates card number with the corresponding regular expression
     *
     * @param cardNum
     * @return
     */
    public boolean validateCardNum(String cardNum) {
        Pattern pattern = Pattern.compile(CARD_NUM_REG_EX);
        Matcher matcher = pattern.matcher(cardNum);
        return matcher.matches();
    }

    /**
     * Validates comment with the corresponding regular expression
     *
     * @param comment
     * @return
     */
    public boolean validateComment(String comment) {
        Pattern pattern = Pattern.compile(COMMENT_REG_EX);
        Matcher matcher = pattern.matcher(comment);
        return matcher.matches();
    }

    /**
     * Validates house number with the corresponding regular expression
     *
     * @param houseNumber
     * @return
     */
    public boolean validateHouseNumber(String houseNumber) {
        Pattern pattern = Pattern.compile(HOUSE_NUM_REG_EX);
        Matcher matcher = pattern.matcher(houseNumber);
        return matcher.matches();
    }

    /**
     * Validates rating with the corresponding regular expression
     *
     * @param rating
     * @return
     */
    public boolean validateRating(String rating) {
        Pattern pattern = Pattern.compile(RATING_REG_EX);
        Matcher matcher = pattern.matcher(rating);
        return matcher.matches();
    }

    /**
     * Validates car model with the corresponding regular expression
     *
     * @param carModel
     * @return
     */
    public boolean validateCarModel(String carModel) {
        Pattern pattern = Pattern.compile(CAR_MODEL_REG_EX);
        Matcher matcher = pattern.matcher(carModel);
        return matcher.matches();
    }
}
