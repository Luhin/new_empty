package com.htp.taxi.listener;

import com.htp.taxi.command.AttributeName;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Random;

/**
 * Creates page unique index, when session will be start. Application Lifecycle
 * Listener implementation class SessionStratListneter
 *
 */
@WebListener
public class SessionStartListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent sessionEvent) {
        int pageUnique = new Random().nextInt();
        sessionEvent.getSession().setAttribute(AttributeName.PAGE_UNIQUE, pageUnique);
    }

    public void sessionDestroyed(HttpSessionEvent sessionEvent) {
    }

}
