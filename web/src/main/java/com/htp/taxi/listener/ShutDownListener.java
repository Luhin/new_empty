package com.htp.taxi.listener;

import com.htp.taxi.dao.HIbernateSessionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class ShutDownListener
 */
@WebListener
public class ShutDownListener implements ServletContextListener {
    private static Logger logger = LogManager.getLogger();

    /**
     * Closes all connections from the ConnectionPool, when the application will
     * be shut down
     *
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        try {
            HIbernateSessionManager.getInstance().shutdown();
        } catch (HibernateException e) {
            logger.error("Session factory close error", e);
        }
    }

    /**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
    }
}
