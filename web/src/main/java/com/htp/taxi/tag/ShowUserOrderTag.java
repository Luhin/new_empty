package com.htp.taxi.tag;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.Order;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with user orders, and creates corresponding table
 *
 * @see javax.servlet.jsp.tagext.TagSupport
 *
 * @author Uladzimir Luhin
 *
 */
public class ShowUserOrderTag extends TagSupport {
    private static final long serialVersionUID = -4235625769575067233L;
    private final static String DATE_FORMAT = "yyyy-mm-dd hh:mm:ss";
    private final static String MONEY_FORMAT = "00.00 BYN";
    private List<Order> orderList;
    private ResourceBundle bundle;

    public void setOrderList(List<Order> list) {
        this.orderList = list;
    }

    @Override
    public int doStartTag() throws JspException {
        String userLocale = (String) pageContext.getSession().getAttribute(AttributeName.LOCALE);
        Locale locale;
        if (userLocale == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(userLocale);
        }
        bundle = ResourceBundle.getBundle("localization.locale", locale);
        String noRides = bundle.getString("locale.message.no.user.order");
        String date = bundle.getString("locale.message.ride.date");
        String position = bundle.getString("locale.destination.from");
        String destination = bundle.getString("locale.destination.to");
        String subjectName = null;
        String driverComment = null;
        String clientComment = null;
        if (pageContext.getSession().getAttribute(AttributeName.CLIENT) instanceof Client) {
            subjectName = bundle.getString("locale.message.driver.name");
            driverComment = bundle.getString("locale.message.driver.comment");
            clientComment = bundle.getString("locale.message.my.comment");
        }
        if (pageContext.getSession().getAttribute(AttributeName.CLIENT) instanceof Driver) {
            subjectName = bundle.getString("locale.message.client.name");
            driverComment = bundle.getString("locale.message.my.comment");
            clientComment = bundle.getString("locale.message.client.comment");
        }
        String price = bundle.getString("locale.message.price");
        String action = bundle.getString("locale.message.action");
        String editMyComment = bundle.getString("locale.button.edit.comment");
        try {
            JspWriter out = pageContext.getOut();
            if (orderList == null || orderList.isEmpty()) {
                out.write(noRides);
            } else {
                out.write("<table class='table table-bordered table-condensed'><tr><th>" + date + "</th><th>" + position
                        + "</th><th>" + destination + "</th><th>" + subjectName + "</th><th>" + price + "</th><th>"
                        + driverComment + "</th>" + "<th>" + clientComment + "</th><th>" + action + "</th></tr>");
                for (Order order : orderList) {
                    out.write("<tr><td>");
                    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
                    out.write(sdf.format(new Date(order.getDate()).getTime()));
                    out.write("</td><td>");
                    out.write(order.getPositionAddress().getStreetName());
                    out.write(" ");
                    out.write(Integer.toString(order.getPositionHouse()));
                    out.write("</td><td>");
                    out.write(order.getDestinationAddress().getStreetName());
                    out.write(" ");
                    out.write(Integer.toString(order.getDestinationHouse()));
                    out.write("</td><td>");
                    out.write(order.getDriver().getName());
                    out.write("</td><td>");
                    DecimalFormat decimalFormat = new DecimalFormat(MONEY_FORMAT);
                    String formattedPrice = decimalFormat.format(order.getPrice());
                    out.write(formattedPrice);
                    out.write("</td><td>");
                    if (order.getDriverComment() != null) {
                        out.write(order.getDriverComment());
                    }
                    out.write("</td><td>");
                    if (order.getClientComment() != null) {
                        out.write(order.getClientComment());
                    }
                    out.write("</td><td>");
                    out.write(
                            "<form action='Controller' method='post'><input type='hidden' name='command' value='editing' /><input type='hidden' name='orderId' value='"
                                    + order.getOrderId() + "'> <input class='btn' type='submit' value='" + editMyComment
                                    + "' /></form>");
                    out.write("</td></tr>");
                }
                out.write("</table>");
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
