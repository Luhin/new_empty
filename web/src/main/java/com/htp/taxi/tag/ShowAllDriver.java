package com.htp.taxi.tag;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with drivers, and creates corresponding table
 *
 * @see javax.servlet.jsp.tagext.TagSupport
 *
 * @author Uladzimir Luhin
 *
 */
public class ShowAllDriver extends TagSupport {
    private static final long serialVersionUID = -3387513524742243377L;
    private final static String LOCALE_PROPERTIES = "localization.locale";
    private List<Driver> driverList;
    private ResourceBundle bundle;

    public void setDriverList(List<Driver> list) {
        this.driverList = list;
    }

    @Override
    public int doStartTag() throws JspException {
        String userLocale = (String) pageContext.getSession().getAttribute(AttributeName.LOCALE);
        Locale locale;
        if (userLocale == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(userLocale);
        }
        bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
        String noDrivers = bundle.getString("locale.message.no.drivers");
        String driverName = bundle.getString("locale.message.driver.name");
        String driverRating = bundle.getString("locale.message.client.rating");
        String eMail = bundle.getString("locale.message.email");
        String banned = bundle.getString("locale.message.banned");
        String notBanned = bundle.getString("locale.message.not.banned");
        String ban = bundle.getString("locale.button.ban");
        String unban = bundle.getString("locale.button.unban");
        String status = bundle.getString("locale.message.status");
        String carModel = bundle.getString("locale.message.driver.car");
        String action = bundle.getString("locale.message.action");
        int pageUnique = (int) pageContext.getSession().getAttribute(AttributeName.PAGE_UNIQUE);
        try {
            JspWriter out = pageContext.getOut();
            if (driverList == null || driverList.isEmpty()) {
                out.write(noDrivers);
            } else {
                out.write("<table class='table table-bordered table-condensed' style='width: 60%'><tr><th>" + driverName
                        + "</th><th>" + driverRating + "</th><th>" + carModel + "</th><th>" + eMail + "</th><th>"
                        + status + "</th><th>" + action + "</th></tr>");
                for (Driver user : driverList) {
                    out.write("<tr><td>");
                    out.write(user.getName());
                    out.write("</td><td>");
                    out.write(user.getFormattedRating());
                    out.write("</td><td>");
                    out.write(user.getCarModel());
                    out.write("</td><td>");
                    out.write(user.geteMail());
                    out.write("</td><td>");
                    if (user.isBanned()) {
                        out.write(banned);
                        out.write("</td><td>");
                        out.write(
                                "<form action='Controller' method='post'><input type='hidden' name='command' value='unban_user' /><input type='hidden' name='pageUnique' value='"
                                        + pageUnique + "' /><input type='hidden' name='userId' value='"
                                        + user.getUserId() + "'> <input class='btn btn-info' type='submit' value='"
                                        + unban + "' /></form>");
                        out.write("</td></tr>");
                    } else {
                        out.write(notBanned);
                        out.write("</td><td>");
                        out.write(
                                "<form action='Controller' method='post'><input type='hidden' name='command' value='ban_user' /><input type='hidden' name='pageUnique' value='"
                                        + pageUnique + "' /><input type='hidden' name='userId' value='"
                                        + user.getUserId() + "'> <input class='btn btn-primary' type='submit' value='"
                                        + ban + "' /></form>");
                        out.write("</td></tr>");
                    }
                }
                out.write("</table>");
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
