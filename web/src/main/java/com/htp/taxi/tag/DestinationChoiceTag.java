package com.htp.taxi.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with all available addreses, and helps user to choise
 * destination
 *
 * @see javax.servlet.jsp.tagext.TagSupport
 *
 * @author Uladzimir Luhin
 *
 */
public class DestinationChoiceTag extends TagSupport {
    private static final long serialVersionUID = -2257382606512853502L;
    private ArrayList<String> streetList;

    public void setStreetList(ArrayList<String> streetList) {
        this.streetList = streetList;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();
            out.write("<select name='destination'>");
            for (String street : streetList) {
                out.write("<option>");
                out.write(street);
                out.write("</option>");
            }
            out.write("</select>");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
