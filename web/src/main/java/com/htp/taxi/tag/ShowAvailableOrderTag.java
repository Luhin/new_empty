package com.htp.taxi.tag;

import com.htp.taxi.command.AttributeName;
import com.htp.taxi.entity.Order;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Implements javax.servlet.jsp.tagext.TagSupport. Creates a tag, which takes as
 * a parameter List with active new orders, and creates corresponding table
 *
 * @author Uladzimir Luhin
 * @see javax.servlet.jsp.tagext.TagSupport
 */
public class ShowAvailableOrderTag extends TagSupport {
    private static final long serialVersionUID = 669631728493468090L;
    private final static String LOCALE_PROPERTIES = "localization.locale";
    private List<Order> orderList;
    private ResourceBundle bundle;

    public void setOrderList(List<Order> list) {
        this.orderList = list;
    }

    @Override
    public int doStartTag() throws JspException {
        String userLocale = (String) pageContext.getSession().getAttribute(AttributeName.LOCALE);
        Locale locale;
        if (userLocale == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(userLocale);
        }
        bundle = ResourceBundle.getBundle(LOCALE_PROPERTIES, locale);
        String noOrders = bundle.getString("locale.order.no.orders");
        String position = bundle.getString("locale.destination.from");
        String destination = bundle.getString("locale.destination.to");
        String clientName = bundle.getString("locale.message.client.name");
        String clientRating = bundle.getString("locale.message.client.rating");
        String acceptOrder = bundle.getString("locale.order.accept");
        String action = bundle.getString("locale.message.action");
        try {
            JspWriter out = pageContext.getOut();
            if (orderList == null || orderList.isEmpty()) {
                out.write(noOrders);
            } else {
                out.write("<table class='table table-bordered table-condensed' style='width:70%'><tr><th>" + position
                        + "</th><th>" + destination + "</th><th>" + clientName + "</th><th>" + clientRating
                        + "</th><th>" + action + "</th></tr>");
                for (Order order : orderList) {
                    out.write("<tr><td>");
                    out.write(order.getPositionAddress().getStreetName());
                    out.write(" ");
                    out.write(Integer.toString(order.getPositionHouse()));
                    out.write("</td><td>");
                    out.write(order.getDestinationAddress().getStreetName());
                    out.write(" ");
                    out.write(Integer.toString(order.getDestinationHouse()));
                    out.write("</td><td>");
                    out.write(order.getClient().getName());
                    out.write("</td><td>");
                    out.write(order.getClient().getFormattedRating());
                    out.write("</td><td>");
                    out.write(
                            "<form action='Controller' method='post'><input type='hidden' name='command' value='acccept_order' /><input type='hidden' name='clientId' value='"
                                    + order.getOrderId() + "'> <input class='btn btn-info' type='submit' value='"
                                    + acceptOrder + "' /></form>");
                    out.write("</td></tr>");
                }
                out.write("</table>");
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
