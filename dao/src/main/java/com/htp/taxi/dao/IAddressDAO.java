package com.htp.taxi.dao;

import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Address;

/**
 * Created by Владимир on 29.11.2016.
 */
public interface IAddressDAO {
    Address determinePosition() throws DAOException;

    Address findAddress(String streetName) throws DAOException;
}
