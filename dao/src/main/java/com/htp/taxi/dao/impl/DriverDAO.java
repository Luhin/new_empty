package com.htp.taxi.dao.impl;

import com.htp.taxi.dao.BaseDAO;
import com.htp.taxi.dao.IDriverDAO;
import com.htp.taxi.entity.Driver;


/**
 * Created by Владимир on 28.11.2016.
 */
public class DriverDAO extends BaseDAO<Driver> implements IDriverDAO {
    @Override
    public Class getPersistentClass() {
        return Driver.class;
    }
}
