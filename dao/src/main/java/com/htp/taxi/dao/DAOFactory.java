package com.htp.taxi.dao;

import com.htp.taxi.dao.impl.*;

/**
 * Created by Владимир on 28.11.2016.
 */
public class DAOFactory {
    private final static DAOFactory INSTANCE = new DAOFactory();
    private UserDAO userDAO = new UserDAO();
    private OrderDAO orderDAO = new OrderDAO();
    private DriverDAO driverDAO = new DriverDAO();
    private ClientDAO clientDAO = new ClientDAO();
    private AddressDAO addressDAO = new AddressDAO();

    private DAOFactory() {
    }

    public static DAOFactory getInstance() {
        return INSTANCE;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public OrderDAO getOrderDAO() {
        return orderDAO;
    }

    public DriverDAO getDriverDAO() {
        return driverDAO;
    }

    public ClientDAO getClientDAO() {
        return clientDAO;
    }

    public AddressDAO getAddressDAO() {
        return addressDAO;
    }
}
