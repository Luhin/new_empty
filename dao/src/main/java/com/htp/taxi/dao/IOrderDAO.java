package com.htp.taxi.dao;

import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Order;
import com.htp.taxi.entity.User;

import java.util.List;

/**
 * Created by Владимир on 28.11.2016.
 */
public interface IOrderDAO {
    List<Order> findUserOrders(User user) throws DAOException;

    void editUserComment(User user, int commentId, String comment) throws DAOException;

    Order findLastOrder(User user) throws DAOException;

    void orderCompleted(Order order) throws DAOException;

}
