package com.htp.taxi.dao;

import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.entity.Client;

/**
 * Created by Владимир on 28.11.2016.
 */
public interface IClientDAO {
    boolean checkCardNumber(Client client, int cardNumber) throws DAOException;
}
