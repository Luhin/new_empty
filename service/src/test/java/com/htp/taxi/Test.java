package com.htp.taxi;


import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.HIbernateSessionManager;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.AddressDAO;
import com.htp.taxi.dao.impl.ClientDAO;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.*;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.impl.ClientService;
import com.htp.taxi.service.impl.DriverService;
import com.htp.taxi.service.impl.OrderService;
import com.htp.taxi.service.impl.UserService;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Before;

import javax.jws.soap.SOAPBinding;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Владимир on 28.11.2016.
 */

public class Test {
    private UserDAO userDAO;
    private ClientDAO clientDAO;

    @Before
    public void init() {
        userDAO = new UserDAO();
        clientDAO = new ClientDAO();
    }

    @org.junit.Test
    public void authorizationTest() throws ServiceException, DAOException {
        UserService service = UserService.getInstance();
        User user = service.authorizeUser("driver", "driver");
        //System.out.println(user.getFormattedRating());

    }

    @org.junit.Test
    public void existLogin() throws ServiceException, DAOException {
        UserService service = UserService.getInstance();
        Assert.assertTrue(userDAO.checkExistLogin("admin"));

    }

    @org.junit.Test
    public void getAll() throws DAOException, ServiceException {
        List<Driver> list = DriverService.getInstance().findAllDrivers();
        System.out.println(list.get(0).getName());
        System.out.println(list.size());
    }

    @org.junit.Test
    public void passTest() throws ServiceException, DAOException {
        UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
        String password = UserService.getInstance().encryptPassword("admin");

    }


    @org.junit.Test
    public void findUserOrder() throws ServiceException {
        Driver driver = (Driver) UserService.getInstance().authorizeUser("driver2", "driver2");
        List<Order> list = OrderService.getInstance().findUserOrder(driver);
        System.out.println(list.get(0).getDriverComment());
    }

    @org.junit.Test
    public void rateOrder() throws ServiceException {
        User user = (User) UserService.getInstance().authorizeUser("user", "user");
        OrderService.getInstance().rateOrder(user, "cococo", "5");
    }

    @org.junit.Test
    public void streetList() throws ServiceException, DAOException {
        List<String> strings = OrderService.getInstance().makeStreetList();
        System.out.println(strings.get(strings.size() - 1));
    }

    @org.junit.Test
    public void json() {
        Order order = new Order();
        order.setTimeCarAwaiting(5);
        order.setPrice(new BigDecimal(34));
        Driver driver = new Driver();
        driver.setName("Vova");
        driver.setFormattedRating("1.1");
        driver.setCarModel("vaz");
        order.setDriver(driver);
        System.out.println(OrderService.getInstance().createAnswerOrder(order));
    }
}
