package com.htp.taxi.service.impl;

import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.AddressDAO;
import com.htp.taxi.dao.impl.ClientDAO;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.Address;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.Rating;
import com.htp.taxi.service.IClientService;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.util.Constants;
import com.htp.taxi.service.util.DataFormatter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

public class ClientService implements IClientService {
    private final static ClientService INSTANCE = new ClientService();

    private ClientService() {
    }

    public static ClientService getInstance() {
        return INSTANCE;
    }

    public Address determineClientPosition() throws ServiceException {
        try {
            AddressDAO addressDAO = DAOFactory.getInstance().getAddressDAO();
            Address address = addressDAO.determinePosition();
            address.setHouseNum(determineClientHouseNumber());
            return address;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public int determineClientHouseNumber() {
        int houseNumber = new Random().nextInt(Constants.MAX_HOUSES);
        return houseNumber;
    }

    public boolean replenishBalance(Client client, int replenishSum, String cardNumber) throws ServiceException {
        try {
            ClientDAO clientDAO = DAOFactory.getInstance().getClientDAO();
            int cardNum = Integer.parseInt(cardNumber);
            if (!clientDAO.checkCardNumber(client, Integer.valueOf(cardNum))) {
                return false;
            } else {
                if (replenishSum >= 100) {
                    UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
                    userDAO.banUser(client.getUserId(), false);
                }
                client.setMoneyAmount(client.getMoneyAmount().add(new BigDecimal(replenishSum)));
                clientDAO.merge(client);
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Client signUpClient(String name, String login, String password, String email, String cardNum)
            throws ServiceException {
        try {
            Client client = new Client();
            client.setName(name);
            client.setLogin(login);
            client.setPassword(UserService.getInstance().encryptPassword(password));
            client.seteMail(email);
            client.setCardNumber(Integer.valueOf(cardNum));
            client.setMoneyAmount(new BigDecimal(0));
            Rating rating = new Rating();
            rating.setMarkCount(1);
            rating.setMarkSum(Constants.START_RATING);
            client.setRating(rating);
            rating.setUser(client);
            ClientDAO clientDAO = DAOFactory.getInstance().getClientDAO();
            int clientId = clientDAO.save(client);
            client.setUserId(clientId);
            return client;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean isBalanceEnough(Client client) {
        return (client.getMoneyAmount().compareTo(new BigDecimal(10))) > 0;
    }

    public List<Client> findAllClients() throws ServiceException {
        List<Client> clientList;
        try {
            ClientDAO clientDAO = DAOFactory.getInstance().getClientDAO();
            clientList = clientDAO.findAll();
            clientList.stream().forEach(client -> {
                String formattedRating = DataFormatter.formatRating(client.getRating().calcRating());
                client.setFormattedRating(formattedRating);
            });
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return clientList;
    }
}
