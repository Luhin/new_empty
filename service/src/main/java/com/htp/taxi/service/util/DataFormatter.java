package com.htp.taxi.service.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by Владимир on 29.11.2016.
 */
public class DataFormatter {
    public static String formatRating(double rating) {
        DecimalFormat decimalFormat = new DecimalFormat(Constants.RATING_FORMAT);
        return decimalFormat.format(rating);
    }

    public static String formatMoney(BigDecimal price) {
        DecimalFormat decimalFormat = new DecimalFormat(Constants.MONEY_FORMAT);
        return decimalFormat.format(price);
    }

    public static String formatTime(int time) {
        DecimalFormat decimalFormat = new DecimalFormat(Constants.TIME_FORMAT);
        return decimalFormat.format(time);
    }
}
