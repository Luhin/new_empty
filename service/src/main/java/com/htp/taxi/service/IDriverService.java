package com.htp.taxi.service;


import com.htp.taxi.entity.Driver;
import com.htp.taxi.entity.User;
import com.htp.taxi.service.exception.ServiceException;

import java.util.List;

/**
 * Makes some service actions, relative to the driver, and then, if necessary,
 * calls correspondig DAO
 *
 * @author Uladzimir Luhin
 */
public interface IDriverService {

    /**
     * Changes drivers car model to the new car model.
     *
     * @param newCar   new car model
     * @throws ServiceException
     */
    boolean changeDriverCar(Driver driver, String newCar, String password) throws ServiceException;

    /**
     * Add a new driver to the database.
     *
     * @param name     driver name
     * @param login    driver login
     * @param password driver password
     * @param email    driver Email
     * @param carModel driver car model
     * @throws ServiceException
     */
    void signUpDriver(String name, String login, String password, String email, String carModel)
            throws ServiceException;

    /**
     * Find all drivers from the database.
     *
     * @return List with all drivers.
     * @throws ServiceException
     */
    List<Driver> findAllDrivers() throws ServiceException;

}
