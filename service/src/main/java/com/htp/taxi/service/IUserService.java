package com.htp.taxi.service;

import com.htp.taxi.entity.User;
import com.htp.taxi.service.exception.ServiceException;

import java.util.List;

/**
 * Makes some service actions, relative to the all users, and then, if
 * necessary, calls corresponding DAO
 *
 * @author Uladzimir Luhin
 */
public interface IUserService {
    /**
     * Authorization of the user in the system.
     *
     * @param login
     * @param password
     * @return if login and password are correct, return object User whith all
     * necessary data.
     * @throws ServiceException
     */
    User authorizeUser(String login, String password) throws ServiceException;

    /**
     * Encryts password by corresponding encoding(MD5 for example)
     *
     * @param password
     * @return String contains encrypted password
     * @throws ServiceException
     */
    String encryptPassword(String password) throws ServiceException;

    /**
     * Check login for the unique. Compare login to the database logins.
     *
     * @param login login which must be checked
     * @return
     * @throws ServiceException
     */
    boolean matchExistLogin(String login) throws ServiceException;

    void banUser(int userId, boolean ban, List<User> userList) throws ServiceException;

    User updateClientData(int clientId) throws ServiceException;


}