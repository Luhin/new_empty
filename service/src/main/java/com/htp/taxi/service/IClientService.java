package com.htp.taxi.service;

import com.htp.taxi.entity.Address;
import com.htp.taxi.entity.Client;
import com.htp.taxi.entity.User;
import com.htp.taxi.service.exception.ServiceException;

import java.util.List;

/**
        * Makes some service actions, relative to the client, and then, if necessary,
        * calls correspondig DAO
        *
        * @author Uladzimir Luhin
        *
        */
public interface IClientService {
    /**
     * Determines current client position.
     *
     * @return String, which contains current street.
     * @throws ServiceException
     */
    Address determineClientPosition() throws ServiceException;

    /**
     * Determines current client house number.
     *
     * @return current client house number.
     */
    int determineClientHouseNumber();

    /**
     * Repelnish balance of a client. Add given sum to the client money amount.
     *
     *            ID of a client
     * @param replenishSum
     *            sum which must be add to the client account
     * @throws ServiceException
     */
    boolean replenishBalance(Client client, int replenishSum, String cardNumber) throws ServiceException;

    /**
     * Add a new client to the database.
     *
     * @param name
     *            client name
     * @param login
     *            client login
     * @param password
     *            client password
     * @param email
     *            client Email
     * @param cardNum
     *            client card number
     * @return ID of a new client
     * @throws ServiceException
     */
    Client signUpClient(String name, String login, String password, String email, String cardNum) throws ServiceException;

    /**
     * Checks client balance, is it enogh for makin a trip
     *
     * @return
     * @throws ServiceException
     */
    boolean isBalanceEnough(Client client);

    /**
     * Find all clients from the database.
     *
     * @return List with all clients.
     * @throws ServiceException
     */
    List<Client> findAllClients() throws ServiceException;

}
