package com.htp.taxi.service.impl;

import com.htp.taxi.dao.DAOFactory;
import com.htp.taxi.dao.exception.DAOException;
import com.htp.taxi.dao.impl.AddressDAO;
import com.htp.taxi.dao.impl.OrderDAO;
import com.htp.taxi.dao.impl.UserDAO;
import com.htp.taxi.entity.*;
import com.htp.taxi.service.IOrderService;
import com.htp.taxi.service.exception.ServiceException;
import com.htp.taxi.service.util.Constants;
import com.htp.taxi.service.util.DataFormatter;
import org.json.simple.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class OrderService implements IOrderService {
    private final static OrderService INSTANCE = new OrderService();

    private OrderService() {

    }

    public static OrderService getInstance() {
        return INSTANCE;
    }

    public List<String> makeStreetList() throws ServiceException {
        try {
            AddressDAO addressDAO = DAOFactory.getInstance().getAddressDAO();
            List<Address> address = addressDAO.findAll();
            List<String> streetList = address.stream()
                    .map(s -> s.getStreetName())
                    .collect(Collectors.toList());
            Collections.sort(streetList);
            return streetList;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void createOrder(Client client, String currentStreet, int currentHouseNumber, String destination,
                            String destHouseNumber) throws ServiceException {
        try {
            AddressDAO addressDAO = DAOFactory.getInstance().getAddressDAO();
            Order order = new Order();
            order.setOrderId(client.getUserId());
            order.setOrderStatus(OrderStatus.NEW);
            order.setClient(client);
            Address positionAddress = addressDAO.findAddress(currentStreet);
            order.setPositionAddress(positionAddress);
            order.setPositionHouse(currentHouseNumber);
            Address destinationAddress = addressDAO.findAddress(destination);
            order.setDestinationAddress(destinationAddress);
            order.setDestinationHouse(Integer.parseInt(destHouseNumber));
            OrderKeeper orderKeeper = OrderKeeper.getInstance();
            orderKeeper.putClientOrder(client.getUserId(), order);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public ArrayList<Order> showOrders() {
        ArrayList<Order> orderList = new ArrayList<>();
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        for (Map.Entry<Integer, Order> entry : orderKeeper.getOrders().entrySet()) {
            Order order = entry.getValue();
            if (order.getOrderStatus() == OrderStatus.NEW) {
                orderList.add(order);
            }
        }
        return orderList;
    }

    public boolean acceptOrder(int clientId, Driver driver) throws ServiceException {
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        Order order = orderKeeper.getClientOrder(clientId);
        if (order == null) {
            return false;
        }
        ReentrantLock orderLock = order.getLock();
        try {
            if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                try {
                    if (order.getOrderStatus() != OrderStatus.NEW) {
                        return false;
                    }
                    order.setDriver(driver);
                    BigDecimal orderPrice = new BigDecimal(new Random().nextInt(Constants.MAX_PRICE) + Constants.SEAT_PRICE);
                    order.setPrice(orderPrice);
                    int carAwaiting = new Random().nextInt(Constants.MAX_CAR_AWAITING_MINUTES) + Constants.MIN_CAR_AWAITING_TIME;
                    order.setTimeCarAwaiting(carAwaiting);
                    order.setOrderStatus(OrderStatus.ACCEPTED);
                    return true;
                } finally {
                    orderLock.unlock();
                }
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            throw new ServiceException("Thread was interrupted", e);
        }
    }

    public void deleteOrder(int userId) throws ServiceException {
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        Order order = orderKeeper.getClientOrder(userId);
        if (order != null) {
            ReentrantLock orderLock = order.getLock();
            try {
                if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                    try {
                        orderKeeper.deleteClientOrder(userId);
                    } finally {
                        orderLock.unlock();
                    }
                }
            } catch (InterruptedException e) {
                throw new ServiceException("Thread was interrupted", e);
            }
        }
    }

    public String checkOrderResult(int orderId) throws ServiceException {
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        int checkCount = 0;
        while (!(checkCount > Constants.ACCEPT_ORDER_AWAITING_SECONDS)) {
            try {
                TimeUnit.MILLISECONDS.sleep(300);
                checkCount++;
            } catch (InterruptedException e) {
                throw new ServiceException("Thread was interrupted", e);
            }
            Order order = orderKeeper.getClientOrder(orderId);
            if (order == null) {
                return Constants.ORDER_CANCELLED;
            }
            ReentrantLock orderLock = order.getLock();
            try {
                if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                    try {
                        if (order.getOrderStatus() == OrderStatus.CONFIRMED) {
                            OrderDAO orderDAO = DAOFactory.getInstance().getOrderDAO();
                            order.setDate(new Date().getTime());
                            order.setOrderId(0);
                            orderDAO.orderCompleted(order);
                            order.setOrderStatus(OrderStatus.DONE);
                            return Constants.SUCCESS_ORDER;
                        }
                    } finally {
                        orderLock.unlock();
                    }
                }
            } catch (InterruptedException | DAOException e) {
                throw new ServiceException("Order wasn't append", e);
            }
        }
        Order order = orderKeeper.getClientOrder(orderId);
        if (order == null) {
            return Constants.ORDER_CANCELLED;
        }
        deleteOrder(orderId);
        return Constants.ORDER_CANCELLED;


    }

    public String waitOrderAcceptance(int clientId) throws ServiceException {
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        int checkCount = 0;
        while (!(checkCount > Constants.ACCEPT_ORDER_AWAITING_SECONDS)) {
            try {
                TimeUnit.MILLISECONDS.sleep(300);
                checkCount++;
            } catch (InterruptedException e) {
                throw new ServiceException("Thread was interrupted", e);
            }
            Order order = orderKeeper.getClientOrder(clientId);
            if (order == null) {
                return Constants.ORDER_CANCELLED;
            }
            ReentrantLock orderLock = order.getLock();
            try {
                if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                    try {
                        if (order.getOrderStatus() == OrderStatus.ACCEPTED) {
                            return createAnswerOrder(order);
                        }
                    } finally {
                        orderLock.unlock();
                    }
                }
            } catch (InterruptedException e) {
                throw new ServiceException("Thread was interrupted", e);
            }
        }
        Order order = orderKeeper.getClientOrder(clientId);
        if (order == null) {
            return Constants.ORDER_CANCELLED;
        }
        deleteOrder(clientId);
        return Constants.NO_CARS;

    }

    public boolean confirmOrder(int orderId) throws ServiceException {
        OrderKeeper orderKeeper = OrderKeeper.getInstance();
        Order order = orderKeeper.getClientOrder(orderId);
        if (order == null) {
            return false;
        }
        ReentrantLock orderLock = order.getLock();
        try {
            if (orderLock.tryLock(1000, TimeUnit.MILLISECONDS)) {
                try {
                    if (order.getOrderStatus() == OrderStatus.ACCEPTED) {
                        order.setOrderStatus(OrderStatus.CONFIRMED);
                        return true;
                    }
                    return false;
                } finally {
                    orderLock.unlock();
                }
            } else {
                return false;
            }

        } catch (InterruptedException e) {
            throw new ServiceException("Thread was interrupted", e);
        }
    }

    public String createAnswerOrder(Order order) {
        JSONObject answer = new JSONObject();
        answer.put(Constants.TIME_AWAITING, DataFormatter.formatTime(order.getTimeCarAwaiting()));
        answer.put(Constants.DRIVER_NAME, order.getDriver().getName());
        answer.put(Constants.DRIVER_RATING, order.getDriver().getFormattedRating());
        answer.put(Constants.DRIVER_CAR_MODEL, order.getDriver().getCarModel());
        answer.put(Constants.ORDER_PRICE, DataFormatter.formatMoney(order.getPrice()));
        return answer.toJSONString();
    }

    public ArrayList<Order> findUserOrder(User user) throws ServiceException {
        try {
            OrderDAO orderDAO = DAOFactory.getInstance().getOrderDAO();
            ArrayList<Order> orderList = (ArrayList) orderDAO.findUserOrders(user);
            return orderList;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void editUserComment(User user, int orderId, String newComment) throws ServiceException {
        try {
            OrderDAO orderDAO = DAOFactory.getInstance().getOrderDAO();
            orderDAO.editUserComment(user, orderId, newComment);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public void rateOrder(User user, String comment, String rating) throws ServiceException {
        try {
            OrderDAO orderDAO = DAOFactory.getInstance().getOrderDAO();
            Order lastOrder = orderDAO.findLastOrder(user);
            UserDAO userDAO = DAOFactory.getInstance().getUserDAO();
            User estimated = null;
            if (user instanceof Driver) {
                estimated = lastOrder.getClient();
            } else {
                estimated = lastOrder.getDriver();
            }
            userDAO.rateUser(estimated, Integer.valueOf(rating));
            orderDAO.editUserComment(user, lastOrder.getOrderId(), comment);

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
