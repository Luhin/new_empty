package com.htp.taxi.service.util;

/**
 * Created by Владимир on 30.11.2016.
 */
public class Constants {
    public final static int MAX_PRICE = 8;
    public final static int SEAT_PRICE = 3;
    public final static int ACCEPT_ORDER_AWAITING_SECONDS = 180;
    public final static int MAX_CAR_AWAITING_MINUTES = 10;
    public final static int MIN_CAR_AWAITING_TIME = 1;
    public final static String NO_CARS = "noCars";
    public final static String ORDER_CANCELLED = "orderCancelled";
    public final static String MONEY_FORMAT = "0.00 BYN";
    public final static String TIME_FORMAT = "0 min";
    public final static String SUCCESS_ORDER = "successOrder";
    public final static int MAX_HOUSES = 150;
    public final static int START_RATING = 4;
    public final static String HASH_FUNCTION = "MD5";
    public final static String TIME_AWAITING = "time";
    public final static String DRIVER_NAME = "name";
    public final static String DRIVER_RATING = "rating";
    public final static String DRIVER_CAR_MODEL = "car";
    public final static String ORDER_PRICE = "price";
    public final static String RATING_FORMAT = "#.#";


    private Constants() {
    }
}
