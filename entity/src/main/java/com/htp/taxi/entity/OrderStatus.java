package com.htp.taxi.entity;

/**
 * Created by Владимир on 10.11.2016.
 */
public enum OrderStatus {
    NEW, ACCEPTED, CONFIRMED, DONE
}
