package com.htp.taxi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Created by Владимир on 24.11.2016.
 */
//CONSTANTS===========!!!!!!!!!!!!!!!!!!!!!?/////////////////////
@Entity
@PrimaryKeyJoinColumn(name = "admin_id")
public class Admin extends User {
    @Column(name = "access_level")
    private int accessLevel;

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }
}
