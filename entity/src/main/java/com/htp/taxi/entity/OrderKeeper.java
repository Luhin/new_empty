package com.htp.taxi.entity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * OrderKeeper keep and manage all active orders in the system.
 *
 * @author Uladzimir Luhin
 */
public class OrderKeeper {
    private static class SingletonHolder {
        private final static OrderKeeper INSTANCE = new OrderKeeper();
    }

    /**
     * @see java.util.concurrent.locks.ReentrantLock
     */
    private static ReentrantLock lock = new ReentrantLock();
    /**
     * Contains all active orders. Key - is clientId, value - Order.
     */
    private ConcurrentHashMap<Integer, Order> orders;

    private OrderKeeper() {
        orders = new ConcurrentHashMap<Integer, Order>();
    }

    public static OrderKeeper getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public ConcurrentHashMap<Integer, Order> getOrders() {
        return orders;
    }

    /**
     * Add a new order to the keeper.
     *
     * @param clientId ID of a client, which will be the key to the order
     * @param order
     */
    public void putClientOrder(Integer clientId, Order order) {
        orders.put(clientId, order);
    }

    public Order getClientOrder(int clientId) {
        Order order = orders.get(clientId);
        return order;
    }

    /**
     * Removes order from the system. Order removes in case success finishing
     * order, or if order was cancelled
     *
     * @param clientId ID of a client - key to the order
     */
    public void deleteClientOrder(int clientId) {
        orders.remove(clientId);
    }
}
