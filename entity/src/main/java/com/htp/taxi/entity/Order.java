package com.htp.taxi.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Order is an object, which contains all necessary information about trip.
 *
 * @author Uladzimir Luhin
 */
@Entity
@Table(name = "ordering")
public class Order extends BaseEntity {
    @Id
    @Column(name = "order_id")
    private int orderId;
    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driver;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @Column
    private BigDecimal price;
    @Column
    private long date;
    @ManyToOne
    @JoinColumn(name = "position")
    private Address positionAddress;
    @Column(name = "position_house")
    private int positionHouse;
    @ManyToOne
    @JoinColumn(name = "destination")
    private Address destinationAddress;
    @Column(name = "dest_house")
    private int destinationHouse;
    @Column(name = "client_comment")
    private String clientComment;
    @Column(name = "driver_comment")
    private String driverComment;
    @Transient
    private int timeCarAwaiting;
    @Transient
    private OrderStatus orderStatus;
    @Transient
    private ReentrantLock lock;

    public Order() {
        lock = new ReentrantLock();
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Address getPositionAddress() {
        return positionAddress;
    }

    public void setPositionAddress(Address positionAddress) {
        this.positionAddress = positionAddress;
    }

    public int getPositionHouse() {
        return positionHouse;
    }

    public void setPositionHouse(int positionHouse) {
        this.positionHouse = positionHouse;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public int getDestinationHouse() {
        return destinationHouse;
    }

    public void setDestinationHouse(int destinationHouse) {
        this.destinationHouse = destinationHouse;
    }

    public String getClientComment() {
        return clientComment;
    }

    public void setClientComment(String clientComment) {
        this.clientComment = clientComment;
    }

    public String getDriverComment() {
        return driverComment;
    }

    public void setDriverComment(String driverComment) {
        this.driverComment = driverComment;
    }

    public int getTimeCarAwaiting() {
        return timeCarAwaiting;
    }

    public void setTimeCarAwaiting(int timeCarAwaiting) {
        this.timeCarAwaiting = timeCarAwaiting;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public ReentrantLock getLock() {
        return lock;
    }

    public void setLock(ReentrantLock lock) {
        this.lock = lock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (orderId != order.orderId) return false;
        if (date != order.date) return false;
        if (positionHouse != order.positionHouse) return false;
        if (destinationHouse != order.destinationHouse) return false;
        if (timeCarAwaiting != order.timeCarAwaiting) return false;
        if (driver != null ? !driver.equals(order.driver) : order.driver != null) return false;
        if (client != null ? !client.equals(order.client) : order.client != null) return false;
        if (price != null ? !price.equals(order.price) : order.price != null) return false;
        if (positionAddress != null ? !positionAddress.equals(order.positionAddress) : order.positionAddress != null)
            return false;
        if (destinationAddress != null ? !destinationAddress.equals(order.destinationAddress) : order.destinationAddress != null)
            return false;
        if (clientComment != null ? !clientComment.equals(order.clientComment) : order.clientComment != null)
            return false;
        if (driverComment != null ? !driverComment.equals(order.driverComment) : order.driverComment != null)
            return false;
        if (orderStatus != order.orderStatus) return false;
        return lock != null ? lock.equals(order.lock) : order.lock == null;

    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + (driver != null ? driver.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (int) (date ^ (date >>> 32));
        result = 31 * result + (positionAddress != null ? positionAddress.hashCode() : 0);
        result = 31 * result + positionHouse;
        result = 31 * result + (destinationAddress != null ? destinationAddress.hashCode() : 0);
        result = 31 * result + destinationHouse;
        result = 31 * result + (clientComment != null ? clientComment.hashCode() : 0);
        result = 31 * result + (driverComment != null ? driverComment.hashCode() : 0);
        result = 31 * result + timeCarAwaiting;
        result = 31 * result + (orderStatus != null ? orderStatus.hashCode() : 0);
        result = 31 * result + (lock != null ? lock.hashCode() : 0);
        return result;
    }
}
