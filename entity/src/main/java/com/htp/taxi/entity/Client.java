package com.htp.taxi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by Владимир on 24.11.2016.
 */
@Entity
@PrimaryKeyJoinColumn(name = "client_id")
public class Client extends User{
    @Column(name = "money_amount")
    private BigDecimal moneyAmount;
    @Column(name = "card_number")
    private int cardNumber;
    @OneToMany(mappedBy = "client")
    private Set<Order> orders;

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(BigDecimal moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        if (!super.equals(o)) return false;

        Client client = (Client) o;

        if (cardNumber != client.cardNumber) return false;
        if (moneyAmount != null ? !moneyAmount.equals(client.moneyAmount) : client.moneyAmount != null) return false;
        return orders != null ? orders.equals(client.orders) : client.orders == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (moneyAmount != null ? moneyAmount.hashCode() : 0);
        result = 31 * result + cardNumber;
        result = 31 * result + (orders != null ? orders.hashCode() : 0);
        return result;
    }
}
